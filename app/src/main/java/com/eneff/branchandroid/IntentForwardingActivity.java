package com.eneff.branchandroid;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

/**
 * The function of this activity is to act as a singular task root for the
 * deep-links to the application.
 * It should always be declared in the manifest with
 * launchMode=singleTask.  This activity allows deep links to be
 * handled in a separate task from the app that hosts the link, without
 * forcing the LaunchActivity to be singleTask,
 * which would destroy its activity stack on launch.
 *
 * It also allows us to easily clear the activity stack when exiting
 * the app or restarting.
 */
public class IntentForwardingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d("***", "ForwardingActivity::onCreate: " + this);
        super.onCreate(savedInstanceState);
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Log.d("***", "ForwardingActivity::onNewIntent: " + this);
        //super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        Log.d("***", "ForwardingActivity::handleIntent: " + this);

        intent.setClass(this, MainActivity.class);

        intent.setFlags(intent.getFlags() & ~Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(intent.getFlags() & ~Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        Log.d("***", "ForwardingActivity::onDestroy: " + this);
        super.onDestroy();
    }
}
